import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

class DigitalQuran extends StatefulWidget {
  _DigitalQuranState createState() => _DigitalQuranState();
}

class _DigitalQuranState extends State<DigitalQuran> {
  final String url = 'https://api.banghasan.com/quran/format/json/surat';
  List data;

  @override
  void initState() {
    super.initState();
    this.getData();
  }

  Future<String> getData() async {
    var res = await http
        .get(Uri.encodeFull(url), headers: {'accept': 'aplication/json'});

    setState(() {
      var content = json.decode(res.body);
      data = content['hasil'];
    });
    return 'success!';
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(10.0),
      child: ListView.builder(
          itemCount: data == null ? 0 : data.length,
          itemBuilder: (BuildContext context, int index) {
            return Container(
              child: Card(
                child: Column(children: <Widget>[
                  ListTile(
                    leading: Text(
                      data[index]['nomor'],
                      style: TextStyle(fontSize: 30.0),
                    ),
                    title: Text(data[index]['nama'],
                        style: TextStyle(
                            fontSize: 25.0, fontWeight: FontWeight.bold)),
                    trailing: Image.asset(
                      data[index]['type'] == 'mekah'
                          ? 'images/mekah.jpg'
                          : 'images/madinah.png',
                      width: 32.0,
                      height: 32.0,
                    ),
                    subtitle: Column(children: <Widget>[
                      Row(children: <Widget>[
                        Text(
                          'Arti : ',
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        Text(
                          data[index]['arti'],
                          style: TextStyle(
                              fontStyle: FontStyle.italic, fontSize: 15.0),
                        ),
                      ]),
                      Row(
                        children: <Widget>[
                          Text(
                            'Jumlah Ayat : ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          Text(
                            data[index]['ayat'],
                          ),
                        ],
                      ),
                      Row(
                        children: <Widget>[
                          Text(
                            'Diturunkan : ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          //DENGAN INDEX type
                          Text(data[index]['type'])
                        ],
                      ),
                    ]),
                  ),
                  ButtonTheme.bar(
                    child: ButtonBar(children: <Widget>[
                      FlatButton(
                          onPressed: () {}, child: const Text("Lihat Detail")),
                      FlatButton(
                          onPressed: () {}, child: const Text("Dengarkan")),
                    ]),
                  )
                ]),
              ),
            );
          }),
    );
  }
}
