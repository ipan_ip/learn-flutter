import 'package:flutter/material.dart';

class Register extends StatefulWidget {
  createState() {
    return RegisterState();
  }
}

class RegisterState extends State<Register> with Validation {

  final formKey = GlobalKey<FormState>();

  String name = "";
  String email = "";
  String password = "";

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(10.0),
      child: Form(
        key: formKey,
        child: Column(
          children: [
            nameField(),
            emailField(),
            passwordField(),
            registerButton()
          ]
        )
      ),
    );
  }

  Widget nameField() {
    return TextFormField(
      decoration: InputDecoration(
        labelText: "Nama Lengkap"
      ),
      validator: validateName,
      onSaved: (String val) {
        name = val;
      },
    );
  }

  Widget emailField() {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        labelText: "Email",
        hintText: "fulan@example.com"
      ),
      validator: validateEmail,
      onSaved: (String val) {
        email = val;
      },
    );
  }

  Widget passwordField() {
    return TextFormField(
      obscureText: true,
      decoration: InputDecoration(
        labelText: "Password",
        hintText: "Enter Password"
      ),
      validator: validatePassword,
      onSaved: (String val) {
        password = val;
      },
    );
  }

  Widget registerButton() {
    return RaisedButton(
      color: Colors.blueAccent,
      onPressed: () {
        if (formKey.currentState.validate()) {
          formKey.currentState.save();
          print('Nama lengkap: $name');
          print('Email: $email');
          print('Password: $password');
        }
      },
      child: Text("Daftar"),
    );
  }
}

class Validation {
  String validatePassword(String password) {
    if (password.length < 4) {
      return "Password minimal 4 karakter";
    }
    return null;
  }

  String validateEmail(String email) {
    if (!email.contains('@')) {
      return 'Email tidak valid';
    }
    return null;
  }

  String validateName(String name) {
    if (name.isEmpty) { 
      return 'Nama Lengkap Harus Diisi'; 
    }
    return null;
  }
}