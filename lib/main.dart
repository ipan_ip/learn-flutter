import 'package:flutter/material.dart';
import './regiser.dart';
import './digital_quran.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> with SingleTickerProviderStateMixin {
  TabController controller;

  @override
  void initState() {
    super.initState();
    controller = TabController(vsync: this, length: 3);
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("Flutter App"),
          backgroundColor: Colors.red[800],
          bottom: TabBar(controller: controller, tabs: <Tab>[
            Tab(
              icon: Icon(Icons.home),
              text: "Home",
            ),
            Tab(
              icon: Icon(Icons.people),
              text: "Register",
            ),
            Tab(
              icon: Icon(Icons.book),
              text: "Al Quran",
            ),
          ]),
        ),
        body: TabBarView(
          controller: controller,
          children: <Widget>[Home(), Register(), DigitalQuran()],
        ),
      ),
    );
  }
}

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.all(20),
        child: Column(
          children: <Widget>[
            Row(children: <Widget>[
              Icon(Icons.archive),
              Text('Artikel Terbaru',
                  style: new TextStyle(fontWeight: FontWeight.bold))
            ]),
            Card(
                child: Column(children: <Widget>[
              Image.network(
                  'https://flutter.io/images/homepage/header-illustration.png'),
              Text('Belajar Flutter')
            ]))
          ],
        ));
  }
}